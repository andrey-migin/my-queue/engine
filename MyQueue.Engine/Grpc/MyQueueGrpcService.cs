using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using MyQueue.Abstractions;
using MyQueue.Domains;

namespace MyQueue.Engine.Grpc
{
    public class MyQueueGrpcService : IMyQueueGrpcService
    {
        public ValueTask CreateQueueAsync(CreateQueueGrpcContract request)
        {
            ServiceLocator.MyQueueInstance.AddQueue(request.QueueId);
            return new ValueTask();
        }

        public async ValueTask EnqueueAsync(IAsyncEnumerable<EnqueueGrpcContract> messages, ServerCallContext context = null)
        {
            var messagesAsDict = await messages.GroupToDictionaryAsync(msg => msg.QueueId);

            foreach (var (queueId, queueMessages) in messagesAsDict)
            {
               var result = ServiceLocator.MyQueueInstance.Enqueue(queueId, 
                    queueMessages.Select(itm => itm.Content));
               
               if (result == MyQueueOperationResult.QueueNotFound)
                   ServiceLocator.Log.WriteLog("<<unknown>>", "Enqueue", context.GetIp(), $"Queue {queueId} not found");
            }
        }

        public IAsyncEnumerable<MyQueueMessage> DequeueAsync(DequeueGrpcContract contract, ServerCallContext context = null)
        {
            var (result, messages) = ServiceLocator.MyQueueInstance.Dequeue(contract.QueueId, contract.MaxAmount,
                contract.CommitSecondsDelay);
            
            if (result == MyQueueOperationResult.QueueNotFound)
                ServiceLocator.Log.WriteLog(contract.AppName, "DequeueMessage", context.GetIp(), $"Queue {contract.QueueId} not found");


            return messages.ToAsyncEnumerable();
        }

        public ValueTask CommitMessagesAsync(CommitMessageGrpcContract request, ServerCallContext context = null)
        {
            var result = ServiceLocator.MyQueueInstance.CommitMessages(request.QueueId, request.IdsToCommit);
            
            if (result == MyQueueOperationResult.QueueNotFound)
                ServiceLocator.Log.WriteLog(request.AppName, "CommitMessage", context.GetIp(), $"Queue {request.QueueId} not found");

            return new ValueTask();
        }
    }
}