using System;
using Grpc.Core;
using Microsoft.AspNetCore.Http;

namespace MyQueue.Engine.Grpc
{
    public static class GrpcExtensions
    {

        private static string TryGetHeaderValue(this HttpRequest request, string headerName)
        {
            return request.Headers.ContainsKey(headerName) 
                ? request.Headers[headerName].ToString() 
                : null;
        }
        
        public static string GetIp(this HttpContext ctx)
        {
            return ctx.Request.TryGetHeaderValue("CF-Connecting-IP")
                   ?? ctx.Request.TryGetHeaderValue("X-Forwarded-For")
                   ?? ctx.Request.HttpContext.Connection.RemoteIpAddress?.ToString() ?? "unknown";
        }

        public static string GetIp(this ServerCallContext context)
        {
            return context == null 
                ? string.Empty 
                : context.GetHttpContext().GetIp();
        }

        
    }
}