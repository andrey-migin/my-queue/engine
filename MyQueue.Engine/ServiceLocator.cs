using Microsoft.Extensions.DependencyInjection;
using MyQueue.Domains;

namespace MyQueue.Engine
{
    public static class ServiceLocator
    {
        
        public static MyQueueInstance MyQueueInstance { get; private set; }
        
        public static Log Log { get; private set; }


        public static void Init(ServiceProvider sp)
        {
            MyQueueInstance = sp.GetRequiredService<MyQueueInstance>();
            Log = sp.GetRequiredService<Log>();
        }
        
    }
}