using System;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using MyQueue.Domains;

namespace MyQueue.Engine.Controllers
{
    public class QueueController : Controller
    {

        [HttpPost("Queue/CreateIfNotExists")]
        public IActionResult CreateIfNotExists([FromQuery] string queueId)
        {
            ServiceLocator.MyQueueInstance.AddQueue(queueId);
            return this.GetActionResult(MyQueueOperationResult.Ok);
        }
        
        
        [HttpPost("Queue/Enqueue/AsBase64")]
        public IActionResult EnqueueAsBase64([FromQuery]string queueId, [FromBody]string[] data)
        {

            var result = ServiceLocator.MyQueueInstance.Enqueue(queueId, 
                data.Select(Convert.FromBase64String));

            return this.GetActionResult(result);
        }
        
        [HttpPost("Queue/Enqueue/AsUtf8String")]
        public IActionResult EnqueueAsUtf8String([FromQuery]string queueId, [FromBody]string[] data)
        {
            
            var result = ServiceLocator.MyQueueInstance.Enqueue(queueId, 
                data.Select(Encoding.UTF8.GetBytes));

            return this.GetActionResult(result);
        }
        
        [HttpGet("Queue/Dequeue/AsBase64")]
        public IActionResult DequeueAsBase64([FromQuery]string queueId, [FromQuery]int secondsTimeout, [FromQuery]int maxMessages)
        {
            var result = ServiceLocator.MyQueueInstance.Dequeue(queueId, maxMessages, secondsTimeout);

            if (result.result != MyQueueOperationResult.Ok)
                return this.GetActionResult(result.result);

            var jsonResult = result.messages.Select(itm => new
            {
                id = itm.Id,
                content = Convert.ToBase64String(itm.Content),
                attemptNo = itm.AttemptNo
            });

            return Json(jsonResult);
        }
        
        [HttpGet("Queue/Dequeue/AsUtf8String")]
        public IActionResult DequeueAsUtf8String([FromQuery]string queueId, [FromQuery]int secondsTimeout, [FromQuery]int maxMessages)
        {
            var result = ServiceLocator.MyQueueInstance.Dequeue(queueId, maxMessages, secondsTimeout);

            if (result.result != MyQueueOperationResult.Ok)
                return this.GetActionResult(result.result);

            var jsonResult = result.messages.Select(itm => new
            {
                id = itm.Id,
                content = Encoding.UTF8.GetString(itm.Content),
                attemptNo = itm.AttemptNo
            });

            return Json(jsonResult);
        }
        
        [HttpPut("Queue/Commit")]
        public IActionResult Commit([FromQuery]string queueId, [FromBody]long[] ids)
        {
            var result = ServiceLocator.MyQueueInstance.CommitMessages(queueId, ids);
            return this.GetActionResult(result);
        }
    }
}