using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyQueue.Domains;

namespace MyQueue.Engine.Controllers
{
    public static class ControllerExtensions
    {
        public static IActionResult GetActionResult(this Controller ctx, MyQueueOperationResult result)
        {
            if (result == MyQueueOperationResult.QueueNotFound)
            {
                ctx.Response.StatusCode = 404;
                return ctx.Content("Queue not found");
            }

            return ctx.Content("OK");
        }
        
    }
}