using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MyQueue.Domains;
using MyQueue.Engine.Grpc;
using ProtoBuf.Grpc.Server;

namespace MyQueue.Engine
{
    public class Startup
    {

        private static IServiceCollection _services;
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCodeFirstGrpc();
            services.AddApplicationInsightsTelemetry();
            services.AddControllers();
            
            services.AddSwaggerDocument(o => { o.Title = "MyNoSqlServer"; });
            
            services.BindMyQueueServices();
            _services = services;

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            
            app.Use((context, next) =>
            {
                if (context.Request.Headers.ContainsKey("X-Forwarded-Proto"))
                    context.Request.Scheme = context.Request.Headers["X-Forwarded-Proto"];
                return next();
            });

            //  app.UseForwardedHeaders();

            
            app.UseStaticFiles();

            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseRouting();
            

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<MyQueueGrpcService>();
                endpoints.MapControllers();
            });
            
            var sp = _services.BuildServiceProvider();
            ServiceLocator.Init(sp);      
        }
    }
}