using System;
using System.Collections.Generic;
using MyQueue.Abstractions;

namespace MyQueue.Domains
{
    public class MyQueueInstance
    {
        private readonly object _lockObject = new ();
        
        private Dictionary<string, MyQueue> _queues = new ();

        public void AddQueue(string queueName)
        {
            lock (_lockObject)
            {
                _queues = _queues.AddToDictionaryByCreatingNewOne(queueName, ()=>new MyQueue());
            }
        }

        public MyQueueOperationResult Enqueue(string queueName, IEnumerable<byte[]> messages)
        {
            if (!_queues.TryGetValue(queueName, out var queue))
                return MyQueueOperationResult.QueueNotFound;

            lock (queue)
                queue.Enqueue(messages);

            return MyQueueOperationResult.Ok;

        }

        public (MyQueueOperationResult result, IReadOnlyList<MyQueueMessage> messages) Dequeue(string queueName, 
            int maxMessagesAmount, int secondsTimeout)
        {
            if (!_queues.TryGetValue(queueName, out var queue))
                return (MyQueueOperationResult.QueueNotFound, Array.Empty<MyQueueMessage>());

            lock (queue)
            {
                var result = queue.Dequeue(maxMessagesAmount, secondsTimeout);
                return (MyQueueOperationResult.Ok, result);
            }
        }
        
        public MyQueueOperationResult CommitMessages(string queueName, IEnumerable<long> ids)
        {
            if (!_queues.TryGetValue(queueName, out var queue))
                return MyQueueOperationResult.QueueNotFound;

            lock (queue)
                queue.CommitMessages(ids);

            return MyQueueOperationResult.Ok;
        }
        
    }
}