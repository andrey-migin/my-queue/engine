﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyQueue.Abstractions;

namespace MyQueue.Domains
{
    public class MyQueue
    {
        private readonly SortedDictionary<long, MyQueueMessage> _messages
            = new();

        private void Enqueue(MyQueueMessage myQueueMessage)
        {
            while (_messages.ContainsKey(myQueueMessage.Id))
            {
                myQueueMessage.Id++;
            }
            
            _messages.Add(myQueueMessage.Id, myQueueMessage);
        }

        public void Enqueue(IEnumerable<byte[]> messages)
        {

            foreach (var message in messages)
            {
                var myQueueMessage = new MyQueueMessage
                {
                    AttemptNo = 0,
                    Content = message,
                    Id = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()
                };

                Enqueue(myQueueMessage);                
            }

        }

        private MyQueueMessage Dequeue(int addSeconds)
        {

            if (_messages.Count == 0)
                return null;

            var (messageTime, value) = _messages.First();

            var now = DateTimeOffset.UtcNow;

            if (messageTime > now.ToUnixTimeMilliseconds())
                return null;

            _messages.Remove(messageTime);

            value.Id = now.AddSeconds(addSeconds).ToUnixTimeMilliseconds();
            value.AttemptNo++;
            Enqueue(value);

            return value;
        }

        public IReadOnlyList<MyQueueMessage> Dequeue(int maxMessages, int addSeconds)
        {
            var amountTaken = 0;

            List<MyQueueMessage> result = null;

            while (amountTaken<maxMessages)
            {
                var nextMessage = Dequeue(addSeconds);
                
                if (nextMessage == null)
                    break;

                result ??= new List<MyQueueMessage>();
                result.Add(nextMessage);
                
                amountTaken++;
            }

            return result ?? Array.Empty<MyQueueMessage>() as IReadOnlyList<MyQueueMessage>;

        }

        public void CommitMessages(IEnumerable<long> messagesToCommit)
        {
            foreach (var messageToCommit in messagesToCommit)
                _messages.Remove(messageToCommit);
        }
    }
}