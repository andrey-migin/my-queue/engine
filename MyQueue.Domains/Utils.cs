using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyQueue.Domains
{
    public static class Utils
    {

        public static Dictionary<TKey, TValue> AddToDictionaryByCreatingNewOne<TKey, TValue>(
            this Dictionary<TKey, TValue> dictionary, TKey key, Func<TValue> getValue)
        {
            return dictionary.ContainsKey(key) 
                ? dictionary 
                : new Dictionary<TKey, TValue>(dictionary) {{key, getValue()}};
        }


        public static async ValueTask<Dictionary<TKey, List<TValue>>> GroupToDictionaryAsync<TKey, TValue>(this IAsyncEnumerable<TValue> src, Func<TValue, TKey> getKey)
        {
            var result = new Dictionary<TKey, List<TValue>>();

            await foreach (var itm in src)
            {
                var key = getKey(itm);
                
                if (!result.ContainsKey(key))
                    result.Add(key, new List<TValue>());
                
                result[key].Add(itm);
            }
                

            return result;
        }
        
    }
}