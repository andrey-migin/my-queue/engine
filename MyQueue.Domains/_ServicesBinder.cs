using Microsoft.Extensions.DependencyInjection;

namespace MyQueue.Domains
{
    public static class ServicesBinder
    {
        public static void BindMyQueueServices(this IServiceCollection sc)
        {
            sc.AddSingleton<MyQueueInstance>();
            sc.AddSingleton<Log>();
        }
        
    }
}