using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;

namespace MyQueue.Domains
{

    public class LogItem
    {
        public long DateTime { get; internal init; }
        public string App { get; internal init; }
        public string Process { get; internal init; }
        public string Message { get; internal init; }
        
        public string Ip { get; internal init; }
    }
    public class Log
    {

        private readonly Queue<LogItem> _messages = new ();

        private IReadOnlyList<LogItem> _asList = Array.Empty<LogItem>();

        public void WriteLog(string appName, string process, string ip, string message)
        {

            lock (_messages)
            {
                var logItem = new LogItem
                {
                    App = appName,
                    Message = message,
                    Process = process,
                    Ip = ip,
                    DateTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()
                };

                _messages.Enqueue(logItem);

                while (_messages.Count > 100)
                {
                    _messages.Dequeue();
                }

                _asList = _messages.ToList();
            }
            
        }

        public IReadOnlyList<LogItem> GetItems()
        {
            return _asList;
        }
    }
}